public interface MoveablePoint {
    void moveUp();
    void moveDown();
    void moveLeft();
    void moveRight();
}
