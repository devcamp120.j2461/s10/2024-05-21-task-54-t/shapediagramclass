public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        Rectangle rectangle1 = new Rectangle(10, 20);
        Rectangle rectangle2 = new Rectangle(30, 40);
        rectangle1.setColor("Do");

        System.out.println(rectangle1.toString());
        System.out.println("Dien tich hinh chu nhat 1 :" + rectangle1.getArea());
        System.out.println(rectangle2.toString());
        System.out.println("Dien tich hinh chu nhat 2 :" + rectangle2.getArea());
        
        
        Triangle triangle1 = new Triangle(60, 10);
        Triangle triangle2 = new Triangle(90, 20);

        System.out.println(triangle1.toString());
        System.out.println("Dien tich hinh tam giac 1: "+triangle1.getArea());
        System.out.println(triangle2.toString());
        System.out.println("Dien tich hinh tam giac 2: "+triangle2.getArea());
    }
}
